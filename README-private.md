## 简介

Base Admin一套简单通用的后台管理系统

这套Base Admin是一套简单通用的后台管理系统，主要功能有：
- 权限管理
- 菜单管理
- 用户管理
- 系统设置
- 实时日志
- 实时监控
- API加密
- 登录用户修改密码、配置个性菜单等

## 技术栈
前端：layui<br/> 
java后端：SpringBoot + Thymeleaf + WebSocket + Spring Security + SpringData-Jpa + MySql<br/> 

## 前往博客查看详情<br/> 
具体介绍请看我的博客[《开源一套简单通用的后台管理系统》](https://www.cnblogs.com/huanzi-qch/p/11534203.html)<br/> 
